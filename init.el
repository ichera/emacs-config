; https://melpa.org/#/getting-started
(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))
(when (< emacs-major-version 24)
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize)

; enable auto-complete (https://github.com/auto-complete/auto-complete)
(ac-config-default)

; C indentation (http://emacswiki.org/emacs/IndentingC)
(setq-default c-basic-offset 4)
(setq c-default-style "linux" c-basic-offset 4)

; auto intentation (emacswiki.org/emacs/AutoIndentation)
(add-hook 'c-mode-common-hook '(lambda () (local-set-key (kbd "RET") 'newline-and-indent)))

; auto ident yanked (pasted) code (same page)
(dolist (command '(yank yank-pop))
  (eval `(defadvice ,command (after indent-region activate)
	   (and (not current-prefix-arg)
		(member major-mode '(emacs-lisp-mode lisp-mode clojure-mode scheme-mode haskell-mode ruby-mode rspec-mode python-mode c-mode c++-mode objc-mode latex-mode plain-tex-mode))
		(let ((mark-even-if-inactive transient-mark-mode)) (indent-region (region-beginning) (region-end) nil))))))

; autopair (https://github.com/capitaomorte/autopair)
(add-to-list 'load-path "~/.emacs.d/loadpath")
(require 'autopair)
(autopair-global-mode)

; yasnippet (https://github.com/capitaomorte/yasnippet)
(add-to-list 'load-path "~/.emacs.d/plugins/yasnippet")
(require 'yasnippet)
(yas-global-mode 1)

; https://truongtx.me/2013/03/10/emacs-setting-up-perfect-environment-for-cc-programming/
(require 'auto-complete-clang)
; FIXME: the following commands from the source material don't work
